package com.ridims.sqlite;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created on 02/06/2016 by
 * Name     : Ari Mahardika Ahmad Nafis
 * Email    : arimahardika.an@gmail.com
 */
public class NameAdapter extends ArrayAdapter<String>{

    public NameAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }
}
